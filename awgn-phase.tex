%We show that phase modulation achieves an information rate with a pre-log of 1/2.
%Even though this result is already known (e.g., see \cite{Blachman1953}),
%the technique used in the proof is useful for proving other results.
%-------------------------------------------------------------------------------------------------------

%-------------------------------------------------------------------------------------------------------
\begin{lemma} \label{lemma:awgn-lemma-trig}
Let $R$ be a fixed positive real number and let $\Phi \equiv \arg(Y)$ where
\begin{align}
Y = R + Z
\label{eq:ring_Y_def}
\end{align}
and $Z$ is a circularly-symmetric complex Gaussian random variable with mean $0$ and variance $1$.
Then, we have
\begin{align}
\mathbb{E}\left[ \cos(\Phi) \right] &\geq 1 - \frac{1}{R^2}
\label{eq:Ecos} \\
%-------------------------------------------------------
\mathbb{E}\left[ \sin(\Phi) \right] &= 0.
\label{eq:Esin}
\end{align}
\end{lemma}
%-------------------------------------------------------------------------------------------------------

%-------------------------------------------------------------------------------------------------------
\begin{proof}
The probability density function of $\Phi$ is \cite{Aldis1993}
\begin{align}
p_{\Phi}(\phi) 
&= \frac{1}{2 \pi} e^{-\R^2}
+ \frac{1}{\sqrt{4 \pi}} \R \cos(\phi) \ e^{-\R^2 \sin^2\phi}
\erfc\left(- \R \cos\phi \right)
\label{eq:p_Phi}
\end{align}
where $\erfc(\cdot)$ is the complementary error function
\begin{align}
\erfc(z) \equiv \frac{2}{\sqrt{\pi}} \int_z^\infty e^{-t^2} dt.
\label{eq:erfc_def}
\end{align}
Therefore, we have
\begin{align}
&\mathbb{E}\left[ \cos(\Phi) \right] \nonumber\\
&\equiv \int_{-\pi}^{\pi} \cos(\phi) \ p_{\Phi}(\phi) \ d\phi \nonumber\\
%--------------------------------------------------------------------
&\stackrel{(a)}{=} 2 \int_{0}^{\pi} \cos(\phi) \ p_{\Phi}(\phi) \ d\phi \nonumber\\
%--------------------------------------------------------------------
%&\stackrel{(c)}{=} 2 \int_{0}^{\pi} \cos(\phi) \left[
%\frac{1}{2 \pi} e^{-\R^2}
%+ \frac{1}{\sqrt{4 \pi}} \R \cos\phi \ e^{-\R^2 \sin^2\phi}
%\erfc\left(- \R \cos\phi \right)
%\right] d\phi \nonumber\\
%--------------------------------------------------------------------
&\stackrel{(b)}{=} \int_{0}^{\pi} \cos(\phi) \left[
\frac{1}{\sqrt{\pi}} \R \cos(\phi) \ e^{-\R^2 \sin^2\phi}
\erfc\left(- \R \cos\phi \right)
\right] d\phi \nonumber\\
%--------------------------------------------------------------------
%&= \int_{0}^{\pi/2} \cos(\phi) \left[
%\frac{1}{\sqrt{\pi}} \R \cos\phi \ e^{-\R^2 \sin^2\phi}
%\erfc\left(- \R \cos\phi \right)
%\right] d\phi \nonumber \nonumber\\
%&+ \int_{\pi/2}^{\pi} \cos(\phi) \left[
%\frac{1}{\sqrt{\pi}} \R \cos\phi \ e^{-\R^2 \sin^2\phi}
%\erfc\left(- \R \cos\phi \right)
%\right] d\phi \nonumber\\
%--------------------------------------------------------------------
&\stackrel{(c)}{\geq} \int_{0}^{\pi/2} 
\frac{\R}{\sqrt{\pi}} \cos^2(\phi) \ e^{-\R^2 \sin^2\phi}
\erfc\left(- \R \cos\phi \right) d\phi \nonumber\\
%--------------------------------------------------------------------
&\stackrel{(d)}{\geq} \int_{0}^{\pi/2} 
\frac{\R}{\sqrt{\pi}} \cos^2(\phi) \ e^{-\R^2 \sin^2\phi}
\left(2 - e^{-\R^2 \cos^2\phi} \right) d\phi \nonumber\\
%--------------------------------------------------------------------
&= \int_{0}^{\pi/2} \frac{2 \R}{\sqrt{\pi}} \cos^2(\phi) \ e^{-\R^2 \sin^2\phi} d\phi
%- \int_{0}^{\pi/2} \frac{\R}{\sqrt{\pi}} \cos^2\phi \ e^{-\R^2} d\phi \nonumber\\
- \frac{\R}{\sqrt{\pi}} e^{-\R^2} \int_{0}^{\pi/2} \cos^2\phi d\phi \nonumber\\
%--------------------------------------------------------------------
&\stackrel{(e)}{=} \int_{0}^{\pi/2} \frac{2 \R}{\sqrt{\pi}} \cos^2(\phi) \ e^{-\R^2 \sin^2\phi} d\phi
 - \frac{\sqrt{\pi}}{4} \ \R e^{-\R^2}
\label{eq:Ecos_Phi_LB1}
\end{align}
%(a) follows because $p_{\Phi}(\cdot)$ is an even function
%%i.e., $p_{\Phi}(-\phi) = p_{\Phi}(\phi)$
%and $\cos(\phi)$ is also an even function
where
$(a)$ follows because both $p_{\Phi}(\cdot)$ and $\cos(\phi)$ are even functions,
$(b)$ follows from (\ref{eq:p_Phi}) and $\int_0^\pi \cos\phi ~ d\phi = 0$,
$(c)$ follows because the integrand is non-negative over the interval $\phi \in (\pi/2,\pi]$,
%(d) holds because $\erfc\left(- \R \cos\phi \right) = 2 - \erfc\left( \R \cos\phi \right) \geq 2 - e^{-\R^2 \cos^2\phi}$
$(d)$ holds because $\erfc\left(- \R \cos\phi \right) \geq 2 - e^{-\R^2 \cos^2\phi}$ 
(see Lemma \ref{lemma:lowerbounds}) 
and $\cos^2(\phi) \ e^{-\R^2 \sin^2\phi} \geq 0$
and finally 
$(e)$ follows by direct integration.

We bound the integral in (\ref{eq:Ecos_Phi_LB1}) as follows
\begin{align}
&\int_{0}^{\pi/2} \frac{2 \R}{\sqrt{\pi}} \cos^2\phi \ e^{-\R^2 \sin^2\phi} d\phi \nonumber\\
%--------------------------------------------------------------------
&\stackrel{(a)}{\geq} \frac{2 \R}{\sqrt{\pi}} \int_{0}^{\pi/2} (1-\phi^2) \ e^{-\R^2 \phi^2} d\phi \nonumber\\
%--------------------------------------------------------------------
&= \frac{2 \R}{\sqrt{\pi}} \int_{0}^{\pi/2} e^{-\R^2 \phi^2} d\phi 
 - \frac{2 \R}{\sqrt{\pi}} \int_{0}^{\pi/2} \phi^2 \ e^{-\R^2 \phi^2} d\phi \nonumber\\
%--------------------------------------------------------------------
%&\stackrel{(b)}{=} \erf\left( \frac{\pi}{2} R \right)
%- \frac{2 R}{\sqrt{\pi}} \times 
  %\frac{1}{2 R^3} \left[ \frac{\sqrt{\pi}}{2} \erf\left( \frac{\pi}{2} R \right) - \frac{\pi}{2} R \ e^{-\pi^2 R^2/4} \right] \\
%--------------------------------------------------------------------
%&\stackrel{(b)}{=} \erf\left( \frac{\pi}{2} R \right)
%- \frac{1}{R^2} \left[ \frac{1}{2} \erf\left( \frac{\pi}{2} R \right) - \frac{1}{2\sqrt{\pi}} R \ e^{-\pi^2 R^2/4} \right] \\
%--------------------------------------------------------------------
&\stackrel{(b)}{=} \erf\left( \frac{\pi}{2} R \right)
- \frac{1}{2 R^2} \erf\left( \frac{\pi}{2} R \right) + \frac{1}{2\sqrt{\pi} R} e^{-\pi^2 R^2/4} \nonumber\\
%--------------------------------------------------------------------
&\stackrel{(c)}{\geq} \erf\left( \frac{\pi}{2} R \right)
- \frac{1}{2 R^2}
\end{align}
where $\erf(\cdot)$ is the error function defined as
\begin{align}
\erf(z) \equiv \frac{2}{\sqrt{\pi}} \int_0^z e^{-t^2} dt.
\label{eq:erf_def}
\end{align}
Step 
$(a)$ follows from $\cos^2{\phi} \ e^{-R^2 \sin^2{\phi}} \geq (1-\phi^2) e^{-R^2 \phi^2}$ 
(see Lemma \ref{lemma:lowerbounds}),
$(b)$ follows by using the definition of $\erf(\cdot)$ and \cite[3.321(5)]{Gradshtein2007}:
\begin{align}
\int_0^b t^2 e^{-a^2 t^2} dt
&= \frac{1}{2 a^3} \left[ \frac{\sqrt{\pi}}{2} \erf\left( a b \right) - a b \ e^{-a^2 b^2} \right]
\end{align}
and 
$(c)$ holds because $\erf(\cdot) \leq 1$ and the last term is non-negative.
Substituting back in (\ref{eq:Ecos_Phi_LB1}) yields
\begin{align}
\mathbb{E}\left[ \cos(\Phi) \right] 
%-------------------------------------------------------------------- 
&\geq \erf\left( \frac{\pi}{2} R \right)
- \frac{1}{2 R^2}
- \frac{\sqrt{\pi}}{4} \R \ e^{-\R^2} \\
%-------------------------------------------------------------------- 
&\geq 1 - e^{-\pi^2 R^2/4}
- \frac{1}{2 R^2}
- \frac{\sqrt{\pi}}{4} \R \ e^{-\R^2}
\label{eq:Ecos_Phi_LB}
\end{align}
where the last inequality
%follows from the relation $\erf\left( {\pi R}/{2} \right) =  1 - \erfc\left( {\pi R}/{2} \right) \geq 1 - e^{-\pi^2 R^2/4}$.
follows from the relations $\erf\left( {\pi R}/{2} \right) =  1 - \erfc\left( {\pi R}/{2} \right)$ 
and $\erfc\left( {\pi R}/{2} \right) \leq e^{-\pi^2 R^2/4}$ (see \cite[eq. (5)]{Chiani2002}).
Therefore, we have
\begin{align}
R^2 \mathbb{E}\left[ \cos(\Phi) \right]
&\geq R^2 - \frac{4}{\pi^2 e} - \frac{1}{2}
 - \frac{\sqrt{\pi}}{4} \left(\frac{3}{2 e}\right)^{3/2}
\label{eq:R2_Ecos_Phi_LB}
\end{align}
because 
\begin{align}
 \max_{x} x^m e^{-a x^2}  = \left( \frac{m}{2 a e} \right)^{m/2}
\label{eq:max_power_exp}
\end{align}
for any $a > 0 $ and positive integer $m$,
which implies that
$\R^3 e^{-\R^2} \leq \left({3}/{2 e}\right)^{3/2}$ and
$\R^2 e^{-\pi^2 \R^2/4} \leq {4}/{\pi^2 e}$.
We conclude that
\begin{align}
\mathbb{E}\left[ \cos(\Phi) \right]
\geq 1 - \frac{0.83}{R^2}
\geq 1 - \frac{1}{R^2}.
\label{eq:Ecos_Phi_LB2}
\end{align}

Finally, we have
\begin{align}
\mathbb{E}\left[ \sin(\Phi) \right] 
\equiv \int_{-\pi}^{\pi} \sin(\phi) \ p_{\Phi}(\phi) \ d\phi
= 0
\label{eq:Esin_Phi}
\end{align}
because the integrand is odd.

%Note:
%\begin{align}
%\frac{4}{\pi^2 e} + \frac{1}{2} + \frac{\sqrt{\pi}}{4} \left(\frac{3}{2 e}\right)^{3/2} = 0.83074 \leq 1
%\end{align}
\end{proof}
