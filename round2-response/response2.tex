%\documentclass[a4paper,10pt]{article}
\documentclass{IEEEtran}%
\onecolumn
%\usepackage[utf8x]{inputenc}
%

\usepackage{amsmath}%
\usepackage{amsfonts}%
\usepackage{amssymb}%
\usepackage{graphicx}
\usepackage{epstopdf} % needs to appear after graphicx?
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usepackage[latin1]{inputenc}
\usepackage{verbatim}

\input{../header}

\newcommand{\alert}[1]{{\color[rgb]{0,0,1} {#1}}}
\newcommand{\codered}[1]{{\color[rgb]{0,0,1} {#1}}}

\begin{document}

\title{RESPONSE}
\maketitle

We remark that all equations, sections, ...etc that we refer to below are with respect to the first revision, unless otherwise stated.

\section{Associate Editor}

Please, address the remaining comments of the reviewers! 
They are mostly minor, but Reviewer 1 still suggests a focused effort on compressing the length of the paper. 
I agree on this and I would strongly encourage you to do it. 
%I will be checking the final files, so please do still provide an answer to the comments.

\alert{

The main change asked for by Reviewer 1 was to shorten the paper. We have done that now by moving the proofs of Theorems 1 and 2 to appendices, as requested. This saves about 4 double-column pages in the main body of the paper, although not for the appendices of course. However, the paper flow is now easier to to follow.

The other suggestion offered was to shorten the numerical calculation section. We assume the reviewer is referring to Section V. However, this section is already rather short, only 3 columns of text. The main space is taken up by 3 Figures. However, we cannot see what we can remove without making the paper scientifically poor. In particular:

\begin{itemize}
\item 
Our basic premise is that a good scientific paper should give enough description to ensure that others can repeat the experiments. To do this, we need at least one of Fig. 6 and/or Fig. 7 to show how the calculation is done. However, neither Fig. 6 or 7 is very large so almost no space is gained.

\item
We need to describe the model (108) for sure. We further must describe Auxiliary Channel 2. One could therefore think of removing Auxiliary Channel 1, but then we would need to add more text that describes why we are converting the continuous set of phases to a finite set. In other words, we would effectively have to introduce the equations surrounding Auxiliary Channel 1 anyway.

\item
So this leaves about 1 column of text starting from (124) to (132). However, without describing the finer Bayesian network that we use (Fig. 7) the reader will not be able to reconstruct our results, which is an essential requirement of any scientific paper.
\end{itemize}


Other changes the reviewer asks for is to add more research results, e.g., on low SNR, bandwidth calculations, etc. Such results are beyond the scope of this paper but will be nice to see in future publications.
}

A few very small issues of my own:
\begin{enumerate}
\item
 bottom line page 2: that grows...

\alert{
Corrected.
}
 
\item
 Fig 2 is not needed.
 
\alert{
Fig 2 is removed from the revised manuscript.
}

\item
 Baud rate is not really needed anywhere.

\alert{
We removed ``Baud rate'' and only use ``symbol rate'' in the revised manuscript.
}
 
\item
 There are several unnumbered equations. I strongly suggest to number them all to ease cross-referencing in case a reader needs to refer to any of those unnumbered equations.
 
\alert{
We followed this suggestion in the revised manuscript.
}

\item
 There is a notation clash with $H(X|HX)$ which should be addressed.

\alert{
We use $H$ and $\H$ to avoid the notational issue in the revised manuscript.
}
 
\end{enumerate}

\section{Reviewers}

We thank the reviewers for their valuable comments. 

\subsection{Reviewer1}

The revised version of the paper is much better written, and it now reads less like an elaboration on the thought process of the authors and more like a proper scientific article with a clear presentation. I commend the authors for their effort and the final result.

As it is, the main results of the paper are 1) lower bounds on the pre-log of the rate of a discrete-time complex AWGN channel with multiplicative Wiener (phase) noise, essentially proving that the pre-log is 3/4 instead of the 1/2 that might have been expected; 2) numerical support of the previous result by means of some simulations of the achievable rates by using mismatched decoding theory. The improvement in pre-log may be achieved by using oversampling at the receiver, which apparently allows to send some residual information over the phase.

\begin{itemize}
\item 
I believe the result in itself is worth publishing, although I am not convinced that it requires 22 single-column pages. My opinion is further supported by the fact that the authors do not seem to introduce any novel analysis or proof technique. In itself, this is no fundamental problem, other than most of the submitted papers are details on how to compute two mismatched decoding rates by choosing a specific auxiliary channel, as well as details on the implementation of the numerical algorithm that estimates the rates. Since I do not believe that they are key to the contribution of the paper, I therefore recommend the authors to revise the paper once again with the objective of reducing its length significantly.

An obvious candidate for saving space is the section on numerical calculations of the rate, where far too many details are given which are not really relevant to the reader of the paper. An obvious candidate for rearrangement is placing the proofs on Theorems 1 and 2 in the appendices.

\codered{
%We believe that after the reorganization of the paper, (especially, following the theorem-proof style), 
%a reader who is not interested in the details can find the results concisely in the two theorems in Sec. IV and Sec. VI.
%A reader interested in the details would also be able to find them in the proofs, Sec. V and the Appendix.
%We do think that shortening the paper further would lead to significant improvement.
Please, see above!
}

\item
In my previous review I had a number of comments on the treatment of the continuous-time model. I appreciate the introduction in the discussion of a brief treatment of the discretisation, although I think it'd be better placed earlier in the paper; a more logical placement is at the transition between continuous-time and discrete-time.

\codered{
We suppose the reviewer is referring here to Remark 7 (in the Discussion section) about the discretization based on the KL-expansion of the phase noise.
We prefer to keep this remark in the discussion rather than the suggested placement at the transition between continuous-time and discrete-time.
We think the flow is not served by introducing a discrete-model based on KL-expansion early in the text, then right away dispense of it and steer the reader to another discrete-time model (oversampling-based model) which we use in the actual analysis.
}

Related to this, there is no discussion on bandwidth, which I believe is a significant weakness as the paper aims at studying the continuous-time model. At least, since there are pulses appearing in the signal model, would it be possible to replace the band-limited AWGN capacity by the corresponding value obtained under a filtering constraint related to the pulse shape? I believe this should be included too.

\codered{
This comment is similar to comment \# 1 of Reviewer 2 from the first round of reviews,
which we have already responded to.
}

\item
As a third point, I would like to see a more integrated discussion of the sufficient statistics within the paper, and how undersampling suggests that the discrete-time model must lose information. Ideally, an estimate of the best possible pre-log should be there, but I understand it is a hard problem.

\codered{
We prefer to keep the discussion on the sufficient statistics in the Discussion section (Remark 7) instead of ``integrating'' it within the paper,
because most of the discussion is centered around the KL-expansion-based model, which is not used elsewhere in the paper.
}

\item
As a fourth point, I do not see why we need Theorems 1 and 2 separately. I would actually ask the authors to come up with a single theorem, which is able to cover both one-sample-per-symbol and multiple samples and which gives in an integrated formula the pre-log as a function of $L$. It is confusing (for the reader) to have two different analyses for a very similar problem, with no clear explanation as to why they are required.

\codered{
\begin{itemize}
\item 
It seems from the wording of this point as if Theorem 1 and Theoreom 2 are results on one-sample-per-symbol and multiple-samples-per-symbol!
We stress that 
Theorem 1 and Theorem 2 are results on amplitude modulation and phase modulation, respectively, and 
that both theorems are for the limiting case of $L \rightarrow \infty$.

\item
The ``integrated formula" the reviewer is asking for can be found in [32].
Barletta and Kramer showed in [32] that for $L=\lceil \SNR^\alpha \rceil$, we have
\begin{equation}\label{eq:prelog}
 \lim_{\SNR\rightarrow\infty} \frac{C(\SNR)}{\log(\SNR)} \geq
 \left\{
 \begin{array}{ll}
  2\alpha, 			 & 0<\alpha\le 1/3 \\
  (1+\alpha)/2,	 & 1/3\le\alpha\le 1/2 \\
  3/4,					 & 1/2 \le \alpha < 1. 
 \end{array}\right.
\end{equation}
We mention that in the new revision.

\item
It is not clear which ``two different analyses'' are referred to here. 
We have one analysis, with two choices of $L$: $L \propto \sqrt{\SNR}$ and $L \propto \sqrt[3]{\SNR}$.
\end{itemize}
}

\end{itemize}


Additionally, I still have a number of simpler issues which may be useful for the authors in preparing their revision:
\begin{enumerate}
\item 
I don't see the point of considering a generic pulse $g(t)$ when most of the analysis deals with square pulses. I would prefer to consider square pulses only since there is not proper discussion on the effect of having other shapes. The current discussion on squared-cosine pulses essentially suggests they are not very useful, in which case I wonder why they should be included.


\codered{
This comment is the same as ``simple issue'' \# 1 of Reviewer 2 from the first round of reviews, which we have already responded to.
}

\item 
As before, I am not sure all the references you mention are really relevant to this problem. For instance, in the introduction [6] appears only once, on p. 19; and say [15] and [16] are only cited in the introduction. This suggests to be that they are actually not very relevant to the theorems proved in the paper, so I encourage the authors to go over all the references critically and remove unneeded ones.

\codered{
We believe that [6], [15] and [16] are relevant. Just because a reference is cited once in the introduction does not mean it is ``not very relevant''.
}

\item 
On the references, I miss a deeper discussion against the literature on continuous-time phase noise modelling.

\codered{
The physical origin of phase noise and how to model it mathematically is not the topic of our study 
and therefore, we think that no ``deep'' discussion is needed.
We have already included some references (e.g., [4]) that are concerned only with modeling of phase noise.
}

\item 
I suggest to merge Sections II and III into a single channel and receiver model section, including the possible oversampling. It is still too disconnected, with functions (e.g. pulse shapes, sampling factor, matched filter versus integrator) defined only to be changed in the following paragraph. For instance, I would like to have the overall analysis be valid for both $L = 1$ and $L > 1$; I see no logical reason why it would not be possible.

\codered{
\begin{itemize}
\item 
We do not understand why the Reviewer thinks that we change the functions from Section II, they are all the same. Perhaps the Reviewer means ``specialized'' rather than ``changed''? However, it is common in good scientific writing to start general and then specialize. Furthermore, the information theory literature usually describes a channel model by focusing on the unknown parameters, in particular the noise (here phase noise and AWGN). Also, it seems more logical to us to keep the channel model (Section II) separate from how we use the channel (Section III). In any case, this seems to be a minor point.
\item 
Our analysis is indeed \emph{valid} for $L = 1$ and $L > 1$.
One needs to look no further than the lower bounds (73) and (105) to see that they are valid for $L \geq 1$.
However, the bounds yield interesting results in the limit of large $L$.
For $L = 1$, the bounds lead to a trivial results, e.g., pre-log $\geq 0$, which is valid.
\end{itemize}
}

\item 
I would state a clear definition of the bandwidth (or pulse shape constraint) and compare the results to the capacity under that constraint, including how phase noise prevents us from having a one-sample-per-symbol sufficient statistics of the input signal.

\codered{
Here the reviewer asks to add more research results. Such results are beyond the scope of this paper but will be nice to see in future publications. 
We do not consider bandwidth issues because we are computing capacity bounds and not spectral efficiency bounds (where one normalizes by the bandwidth). Capacity results are fine when one is not sharing a medium, which often occurs in, e.g., wired communications. 
}

\item 
On the Auxiliary channels, the authors could also directly state the GMI rate with an optimising parameter $s$, which is to be adjusted. As an advantage, the ``channel" need not be a normalised pdf, it just has to be integrable. This may simplify a bit the analysis. Moreover, the parameter alpha in the proof of Theorem 2, which seems a bit mysterious, would be subsumed in the analysis, making it clearer.

\codered{
We do not think there is mystery about the parameter $\alpha$ -- it is an optimization parameter, just as the suggested $s$ would be. 
In fact, by inspecting (105), one sees that the optimal $\alpha$ is essentially the sum of 
the phase noise variance per sample and the (normalized) additive noise variance per sample,
which is quite meaningful.
}

\item 
I suggest to remove the notation $I(X;Y)$, $I(X_A;Y)$ and $I(\Phi_X;Y|X_A)$ and replace them by $R(\SNR)$, $R_A(\SNR)$, and $R_{\Phi|A}(\SNR)$, they are clearer in the context of the paper since the channel is not stationary memoryless, and the single-letter notation used is a bit misleading.

\codered{
The ``single letter'' notation $\H(X)$ $I(X;Y)$ etc. for entropy rate and information rate is standard in the information theory literature. We refer the reviewer to the Wikipedia page on "Entropy Rate" and many text books on information theory (see also J.L. Massey's course notes that are available via an ETH web site). We prefer to use standard notation.
}

\item 
As mentioned above, I suggest to remove the cosine-squared pulse as well as Figs 9 and 10. They are redundant and their content can just be briefly commented in the paper.

\codered{
This comment is the same as ``simple issue'' \#15 of Reviewer 2 from the first round of reviews, which we have already responded to.
}

\end{enumerate}

\subsection{Reviewer2}

The authors have address all my comments to my full satisfaction. Most importantly, they have reorganized the whole paper, making it more concise and easier to read. I still have a number of minor comments, but believe that the authors can address these when preparing the final manuscript. I am therefore happy to recommend to accept the paper for publication in the IEEE Transactions on Information Theory.

Some minor comments:
\begin{enumerate}
\item 
In the definition of $H_k$ in (14): Shouldn't it be $\Theta(\tau)$ instead of $\theta(\tau)$? Otherwise, $H_k$ is a random variable, but the right-hand side of (14) is deterministic.

\alert{Fixed.}

\item 
At the end of Section III-C, the authors introduce the capacity and achievable rate; see (30) and (31). Strictly speaking, this is not really part of the channel model. Perhaps it would make more sense to introduce these quantities at the beginning of Section IV, or have another subsection at the end of Section III specifically dedicated to capacity and achievable rate?

\codered{
We introduced a subsection at the end of Section III in the revised version.
}

\item 
Theorem 1 on p. 6: It is assumed that $X_A^n$ is i.i.d. However, I believe that before the authors used the index b instead of n. Besides, in the definition of capacity/achievable rate, b tends to infinity. Perhaps it would be more precise to say that the stochastic process $\{X_{A,k}\}$ is i.i.d.? I have a similar comment also for Theorem 2.

\alert{Fixed.}

\item 
First sentence after (46) on p. 7: Shouldn't it be ``$X_{A,1},\ldots,X_{A,b}$" instead of ``$X_{A,1},\ldots,X_{A,n}$"?

\alert{Fixed.}

\item 
Between (58) and (59): The authors use $\tilde{G}\leq 1$ to obtain (59). However, they upper bound $\mathbb{E}[G]\leq 1$ in (57). Shouldn't this be $G\leq 1$?

\alert{Fixed.}

\item 
Equation (109) on p. 13: Shouldn't it be $I(X^b;Y^b)$ instead of $I(X^n;Y^n)$?

\codered{No. We remark that it is $I(\Xsamp^n;Y^n)$ not $I(X^n;Y^n)$.} 

\item 
Section V, pp. 13-16: Shouldn't the sequences $x^n, y^n, \theta^n$, etc. be of length $b$ rather than of length $n$? Or if the authors consider sequences of length $n$, then one should normalize the right-hand side of (111) by $n$ rather than by $b$, shouldn't one?

\codered{No. The equations are correct $x^n, y^n, \theta^n$ are oversampled, so $n$ is the correct length. 
As for the normalization, $b$ is the right length because there are only $b$ transmitted symbols.} 

\item 
p. 14, paragraph after (116), third line: ``We approach this issue by first computing..." (Remove ``is".)

\alert{Fixed.}

\item 
Figures 9 and 10 on pp.17/18: Perhaps one could omit the curves for $S=64$ and merely mention that these curves are indistinguishable from those for $S=32$. Like this, the plots may be easier to read.

\codered{
We prefer to keep the curves for $S=64$.
}

\item 
Section VI-C: The authors refer to the channel model (15) as the ``approximate Baud-rate model". However, in Section III-B they introduced this model as the ``approximate discrete-time model" or the ``approximate symbol-rate model". I found this a bit confusing. Perhaps it would be better to refer to the channel model (15) throughout the entire paper as the ``approximate Baud-rate model". Otherwise readers will have to jump back to equation (15) to understand that this is indeed the same as the ``approximate symbol-rate model".

\alert{
In the new revision, we no longer use the term ``Baud rate'' and use only the term ``symbol rate''. This should eliminate any confusion.
}

\item 
First two lines on p. 22: The authors write that $\{\Gamma_i\}_{i=0}^{\infty}$ are sufficient statistics for $X$. They then conclude that an infinite number of projections per symbol are needed. One may argue that their argument is only true if most of these $\Gamma_i$'s are indeed meaningful. For example, if only, say, $\Gamma_0$ and $\Gamma_1$ were nonzero, then these two projections would be sufficient statistics of $\{\Gamma_i\}_{i=0}^{\infty}$, in which case a finite number of projections would suffice.

\codered{This is a good point, except that it is not the case at hand. 
In short, the variances of $\Gamma_i$'s are non-zero, because $\lambda_i$'s are non-zero.
It is true that $\lambda_i \rightarrow 0$ as $i \rightarrow \infty$, but $\lambda_i$ is not strictly zero for finite $i$.
}

\item 
Second line on p. 22: ``...we believe that a finite subset of $\{\Gamma_i\}_{i=0}^{\infty}$ captures..." (That is, remove "can".)

\alert{Fixed.}

\item 
6th line on p. 22: ``...the information rate in 9 against $E_b/N_0$...". What is ``9"?

\alert{Fixed.}

\item 
Before (157) on p. 23: ``Then, we have"

\alert{Fixed.}

\end{enumerate}



\subsection{Reviewer3}

All my remarks have been addressed satisfactorily. Hence, I recommend the acceptance of the manuscript.

Few minor issues I would like the authors to take care of before publications are listed below:
\begin{enumerate}
\item 
Page 2: Please explain which approximation is involved in the ``approximate multi-sample model" the first time this model is mentioned.

\alert{We follow the suggestion in the new revision.}

\item
Page 3: ``Martalo et al" should be ``Martal\`{o} et al."

\alert{Fixed.}

\item
Page 4: I would not state any conjecture here about the process $\{H_k\}$.

\codered{
We do not phrase our statement as a conjecture in the revised version.
}

\item
Page 4: It would probably be worth mentioning again at the end of Section III.B that if the process $\{\theta(k T_s)\}$ has finite differential entropy rate, then the pre-log is 1/2.

\item
Page 6: The operators $|\cdot|$ and $\arg(\cdot)$ are applied to vectors even if they are defined as function taking scalar values as inputs. So perhaps it would be worth explaining that they operate element-wise on a vector.

\codered{
%We do not apply $|\cdot|$ and $\arg(\cdot)$ to vectors, e.g., we never write $|X^b|$ or $\arg(X^b)$. 
We suppose the reviewer read, for example, $X_A^b$ as $|X^b|$ while we intended it to be read as $|X|^b$.
To avoid ambiguity, we mention explicitly in the new revision that
$X_A^\nsymb = (|X_1|,\ldots,|X_\nsymb|)$ and $\Phi_X^\nsymb = (\arg{X_1},\ldots,\arg{X_\nsymb})$.
}

\item
Page 6: After (43), you should say explicitly that the pre-log was found in [32] to be at least 3/4. Also the remark 4 on page 21 (which is formulated in a rather ambiguous way) seems to be superfluous and can be removed.

\alert{
We mention in the new revision that it was shown that the pre-log was at least 3/4 for the full model.
We also remove Remark 4.
}

\item
Page 10: Please mention that some intuition behind the choice of $\tilde{\Psi}_k$ is provided in Section VII, remark 3.

\alert{We follow the suggestion in the new revision.}

\item
Page 12: Towards the end of the page, the two sentences starting with ``This may result. . . ", and ``For large $L$,..." need some polishing.

\codered{
The wording is slightly changed in the revised version.
}

\item
Page 14: In the sentence ``We approach this issue is by first..." remove ``is".

\alert{Fixed.}

\item
Page 20: On the third line after (142), $\tilde{Y}_k$ should be replaced with $\tilde{\Psi}_k$.

\alert{Fixed.}

\item
Page 21: In remark 6, please clarify how the result was specialized in [42].

\codered{
We clarify in the revised version that [42] studies the special case without additive noise.
}

\item
Page 22: I do not see the point of remark 8. I suggest to remove it.

\codered{
We added this remark based on the request of another reviewer.
}

\item
Page 25: Please replace [47,eq(5)] by [47, Eq. (5)].

\alert{Changed [47,eq(5)] to [47, eq. (5)].}

\end{enumerate}

%\bibliographystyle{alpha}
%%\bibliography{bibstrings_short,dissert3_short}
%\bibliography{bibstrings_short,dissert4_short}

\end{document}

